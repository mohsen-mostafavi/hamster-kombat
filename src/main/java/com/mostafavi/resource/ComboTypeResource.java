package com.mostafavi.resource;

import com.mostafavi.dto.CreateComboTypeDTO;
import com.mostafavi.dto.UpdateComboTypeDTO;
import com.mostafavi.service.ComboTypeService;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;

@Path("/combo-type")
public class ComboTypeResource {
    @Inject
    ComboTypeService comboTypeService;

    @GET
    public Response getAllComboType() {
        return Response.ok(comboTypeService.getAllComboType()).build();
    }

    @POST
    public Response createComboType(CreateComboTypeDTO createComboTypeDTO) {
        comboTypeService.createComboType(createComboTypeDTO);
        return Response.ok().build();
    }
    @PUT
    public Response updateComboType(UpdateComboTypeDTO updateComboTypeDTO) {
        comboTypeService.updateComboType(updateComboTypeDTO);
        return Response.ok().build();
    }
    @DELETE
    @Path("/{id}")
    public Response deleteComboType(@PathParam("id") Long id) {
        comboTypeService.deleteComboType(id);
        return Response.ok().build();
    }

}
