package com.mostafavi.resource;

import com.mostafavi.dto.CreateComboLevelDTO;
import com.mostafavi.service.UserComboLevelService;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

@Path("/user-combo-level")
public class UserComboLevelResource {
    @Inject
    UserComboLevelService userComboLevelService;
    @GET
    public Response getAllUserComboLevelByUserId(@QueryParam("userId") Long userId) {
        return Response.ok(userComboLevelService.findAllUserComboLevelByUserId(userId)).build();
    }
    @POST
    public Response createUserComboLevel(@QueryParam("userId") Long userId,
                                         CreateComboLevelDTO createComboLevelDTO) {
        userComboLevelService.createUserComboLevel(userId, createComboLevelDTO);
        return Response.ok().build();
    }
}
