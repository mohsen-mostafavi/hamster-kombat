package com.mostafavi.resource;

import com.mostafavi.service.UserService;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;

@Path("/user")
public class UserResource {
    @Inject
    UserService userService;
    @GET
    @Path("/{name}")
    public Response getUserByName(@PathParam("name") String name){
        return Response.ok(userService.findByName(name)).build();
    }

    @POST
    public Response createUser(String name){
        userService.create(name);
        return Response.ok().build();
    }

}
