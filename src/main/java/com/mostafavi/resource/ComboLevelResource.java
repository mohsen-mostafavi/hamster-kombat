package com.mostafavi.resource;

import com.mostafavi.dto.CreateComboLevelDTO;
import com.mostafavi.dto.UpdateComboLevelDTO;
import com.mostafavi.service.ComboLevelService;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;

@Path("/combo-level")
public class ComboLevelResource {
    @Inject
    ComboLevelService comboLevelService;
    @GET
    public Response getAllComboLevel(@QueryParam("comboId") Long comboId,
                                     @QueryParam("level") Integer level) {
        return Response.ok(comboLevelService.findComboLevels(comboId,level)).build();
    }

    @POST
    public Response createComboLevel(CreateComboLevelDTO createComboLevelDTO) {
        comboLevelService.create(createComboLevelDTO);
        return Response.ok().build();
    }

    @PUT
    public Response updateComboLevel(UpdateComboLevelDTO updateComboLevelDTO) {
        comboLevelService.update(updateComboLevelDTO);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteComboLevel(@PathParam("id") Long id) {
        comboLevelService.delete(id);
        return Response.ok().build();
    }
}
