package com.mostafavi.resource;

import com.mostafavi.dto.ComboCreateDTO;
import com.mostafavi.dto.ComboUpdateDTO;
import com.mostafavi.service.ComboService;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;

@Path("/combo")
public class ComboResource {
    @Inject
    ComboService comboService;
    @GET
    public Response getAllCombos(@QueryParam("type") Long type) {
        if (type!=null)
            return Response.ok(comboService.findByType(type)).build();
        else
            return Response.ok(comboService.findAll()).build();
    }
    @GET
    @Path("/{id}")
    public Response getComboById(@PathParam("id") Long id) {
        return Response.ok(comboService.findById(id)).build();
    }

    @POST
    public Response createCombo(ComboCreateDTO comboCreateDTO) {
        comboService.create(comboCreateDTO);
        return Response.ok().build();
    }

    @PUT
    public Response updateCombo(ComboUpdateDTO comboUpdateDTO) {
        comboService.update(comboUpdateDTO);
        return Response.ok().build();
    }
    @DELETE
    @Path("/{id}")
    public Response deleteCombo(@PathParam("id") Long id) {
        comboService.delete(id);
        return Response.ok().build();
    }
}
