package com.mostafavi.service;

import com.mostafavi.dao.UserDAO;
import com.mostafavi.dto.UserDTO;
import com.mostafavi.entity.User;
import com.mostafavi.mapper.UserMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class UserService {
    @Inject
    UserDAO userDAO;

    public UserDTO findByName(String name) {
        return UserMapper.entityToDTO(userDAO.findByName(name));
    }

    @Transactional
    public void create(String name) {
        userDAO.persist(User.builder().name(name).build());
    }
}
