package com.mostafavi.service;

import com.mostafavi.dao.ComboDAO;
import com.mostafavi.dao.ComboLevelDAO;
import com.mostafavi.dto.ComboLevelDTO;
import com.mostafavi.dto.CreateComboLevelDTO;
import com.mostafavi.dto.UpdateComboLevelDTO;
import com.mostafavi.entity.Combo;
import com.mostafavi.entity.ComboLevel;
import com.mostafavi.mapper.ComboLevelMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;

@ApplicationScoped
public class ComboLevelService {
    @Inject
    ComboLevelDAO comboLevelDAO;
    @Inject
    ComboDAO comboDAO;

    @Transactional
    public void create(CreateComboLevelDTO createComboLevelDTO) {
        Combo combo = comboDAO.findById(createComboLevelDTO.comboId());
        comboLevelDAO.persist(ComboLevelMapper.dtoToEntity(createComboLevelDTO, combo));
    }

    @Transactional
    public void update(UpdateComboLevelDTO updateComboLevelDTO) {
        ComboLevel comboLevel = comboLevelDAO.findById(updateComboLevelDTO.id());
        comboLevel.setLevel(updateComboLevelDTO.level());
        comboLevel.setPrice(updateComboLevelDTO.price());
        comboLevel.setProfit(updateComboLevelDTO.profit());
        comboLevelDAO.persist(comboLevel);
    }

    @Transactional
    public void delete(Long id) {
        comboLevelDAO.delete("id", id);
    }

    public List<ComboLevelDTO> findComboLevels(Long comboId, Integer level) {
        return comboLevelDAO.findByComboIdAndLevel(comboId, level).stream().map(ComboLevelMapper::entityTODTO).toList();
    }
}
