package com.mostafavi.service;

import com.mostafavi.dao.ComboDAO;
import com.mostafavi.dao.ComboLevelDAO;
import com.mostafavi.dao.UserComboLevelDAO;
import com.mostafavi.dao.UserDAO;
import com.mostafavi.dto.CreateComboLevelDTO;
import com.mostafavi.dto.UserComboLevelDTO;
import com.mostafavi.entity.Combo;
import com.mostafavi.entity.ComboLevel;
import com.mostafavi.entity.User;
import com.mostafavi.entity.UserComboLevel;
import com.mostafavi.mapper.ComboLevelMapper;
import com.mostafavi.mapper.UserComboLevelMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;

@ApplicationScoped
public class UserComboLevelService {
    @Inject
    UserComboLevelDAO userComboLevelDAO;
    @Inject
    UserDAO userDAO;
    @Inject
    ComboLevelDAO comboLevelDAO;
    @Inject
    ComboDAO comboDAO;

    public List<UserComboLevelDTO> findAllUserComboLevelByUserId(Long userId) {
        return userComboLevelDAO.findByUserId(userId).stream().map(UserComboLevelMapper::entityTODTO).sorted().toList();
    }

    @Transactional
    public void createUserComboLevel(Long userId, CreateComboLevelDTO createComboLevelDTO) {
        User user = userDAO.findById(userId);
        ComboLevel comboLevel = findComboLevel(createComboLevelDTO);
        UserComboLevel userComboLevel = UserComboLevel.builder()
                .user(user)
                .comboLevel(comboLevel)
                .build();
        userComboLevelDAO.persist(userComboLevel);
    }

    private ComboLevel findComboLevel(CreateComboLevelDTO createComboLevelDTO) {
        ComboLevel comboLevel = comboLevelDAO.findById(createComboLevelDTO.comboLevelId());
        if (comboLevel == null) {
            Combo combo = comboDAO.findById(createComboLevelDTO.comboId());
            comboLevel = ComboLevelMapper.dtoToEntity(createComboLevelDTO, combo);
            comboLevelDAO.persist(comboLevel);
        }
        return comboLevel;
    }
}
