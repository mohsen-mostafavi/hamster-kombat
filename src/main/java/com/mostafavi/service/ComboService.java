package com.mostafavi.service;

import com.mostafavi.dao.ComboDAO;
import com.mostafavi.dto.ComboCreateDTO;
import com.mostafavi.dto.ComboDTO;
import com.mostafavi.mapper.ComboMapper;
import com.mostafavi.dto.ComboUpdateDTO;
import com.mostafavi.entity.Combo;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;

@ApplicationScoped
public class ComboService {
    @Inject
    ComboDAO comboDAO;
    @Inject
    ComboTypeService comboTypeService;

    public List<ComboDTO> findAll() {
        return comboDAO.listAll().stream().map(ComboMapper::entityTODTO).toList();
    }

    public ComboDTO findById(Long id) {
        return ComboMapper.entityTODTO(comboDAO.findById(id));
    }

    @Transactional
    public void create(ComboCreateDTO comboCreateDTO) {
        Combo combo = new Combo();
        combo.setName(comboCreateDTO.name());
        combo.setComboType(comboTypeService.findComboTypeById(comboCreateDTO.comboTypeId()));
        comboDAO.persist(combo);
    }

    @Transactional
    public void update(ComboUpdateDTO comboUpdateDTO) {
        Combo combo = comboDAO.findById(comboUpdateDTO.id());
        combo.setName(comboUpdateDTO.name());
        combo.setComboType(comboTypeService.findComboTypeById(comboUpdateDTO.comboTypeId()));
        comboDAO.persist(combo);
    }

    @Transactional
    public void delete(Long id) {
        comboDAO.delete("id", id);
    }

    public List<ComboDTO> findByType(Long typeId) {
        return comboDAO.findByType(typeId).stream().map(ComboMapper::entityTODTO).toList();
    }
}
