package com.mostafavi.service;

import com.mostafavi.dao.ComboTypeDAO;
import com.mostafavi.dto.ComboTypeDTO;
import com.mostafavi.dto.CreateComboTypeDTO;
import com.mostafavi.dto.UpdateComboTypeDTO;
import com.mostafavi.entity.ComboType;
import com.mostafavi.mapper.ComboTypeMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;

import java.util.List;

@ApplicationScoped
public class ComboTypeService {
    @Inject
    ComboTypeDAO comboTypeDAO;

    public ComboType findComboTypeById(Long id) {
        return comboTypeDAO.findById(id);
    }

    public List<ComboTypeDTO> getAllComboType() {
        return comboTypeDAO.listAll().stream().map(ComboTypeMapper::entityToDTO).toList();
    }

    @Transactional
    public void createComboType(CreateComboTypeDTO createComboTypeDTO) {
        ComboType comboType = new ComboType();
        comboType.setName(createComboTypeDTO.name());
        comboTypeDAO.persist(comboType);
    }

    @Transactional
    public void updateComboType(UpdateComboTypeDTO updateComboTypeDTO) {
        ComboType comboType = comboTypeDAO.findById(updateComboTypeDTO.id());
        comboType.setName(updateComboTypeDTO.name());
        comboTypeDAO.persist(comboType);
    }

    @Transactional
    public void deleteComboType(Long id) {
        comboTypeDAO.delete("id", id);
    }
}
