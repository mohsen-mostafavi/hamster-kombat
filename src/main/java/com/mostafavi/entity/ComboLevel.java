package com.mostafavi.entity;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "combo-level", uniqueConstraints = { @UniqueConstraint(columnNames = { "combo_id","level" }) } )
public class ComboLevel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Combo combo;
    private int level;
    private Long price;
    private Long profit;
    private double rate;
}
