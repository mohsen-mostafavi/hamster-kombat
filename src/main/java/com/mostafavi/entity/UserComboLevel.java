package com.mostafavi.entity;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user-combo-level", uniqueConstraints = { @UniqueConstraint(columnNames = { "user_id","comboLevel_id" }) } )
@NamedQueries({
        @NamedQuery(name = "findByUserId", query = "from UserComboLevel userComboLevel " +
                "inner join userComboLevel.comboLevel comboLevel " +
                "where userComboLevel.user.id = ?1 and comboLevel.level in (select max(comboLevel.level) from ComboLevel comboLevel where comboLevel.combo = userComboLevel.comboLevel.combo)"),
})
public class UserComboLevel implements Comparable<UserComboLevel> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private User user;
    @ManyToOne
    private ComboLevel comboLevel;

    @Override
    public int compareTo(UserComboLevel o) {
        return Integer.compare(this.getComboLevel().getLevel(), o.getComboLevel().getLevel());
    }
}
