package com.mostafavi.utils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

// todo: After Jakarta Data is added to Quarks, this class should be deleted
public class QueryUtil {
    public static String createQuery(Map<String, Object> parameters) {
        return parameters.entrySet().stream()
                .map(entry -> {
                    if (entry.getValue() instanceof Long || entry.getValue() instanceof String)
                        return entry.getKey() + "=:" + entry.getValue();
                    else if (entry.getValue() instanceof List<?>)
                        return entry.getKey() + " in (:" + entry.getValue() + ")";
                    else
                        return entry.getKey() + " in (:" + entry.getValue() + ")";
                })
                .collect(Collectors.joining(" and "));
    }
}
