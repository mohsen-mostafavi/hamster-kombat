package com.mostafavi.dto;

public record ComboLevelDTO(Long id, int level, Long price, Long profit, Long comboId, String comboName) {
}
