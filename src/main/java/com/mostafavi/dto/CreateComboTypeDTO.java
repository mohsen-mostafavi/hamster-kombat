package com.mostafavi.dto;

public record CreateComboTypeDTO(String name) {
}
