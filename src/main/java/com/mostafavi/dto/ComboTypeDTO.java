package com.mostafavi.dto;

public record ComboTypeDTO(Long is, String name) {
}
