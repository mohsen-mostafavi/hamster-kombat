package com.mostafavi.dto;

public record UserComboLevelDTO(Long id, Long userId, String userName, Long comboId, String comboName, String comboType, int level,
                                double rate, Long price, Long profit) implements Comparable<UserComboLevelDTO> {
    public int compareTo(UserComboLevelDTO userComboLevelDTO) {
        return Double.compare(userComboLevelDTO.rate, this.rate);
    }
}
