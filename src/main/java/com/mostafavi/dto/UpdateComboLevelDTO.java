package com.mostafavi.dto;

public record UpdateComboLevelDTO(Long id, int level, Long price, Long profit) {
}
