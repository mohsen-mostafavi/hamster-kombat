package com.mostafavi.dto;

public record ComboUpdateDTO(Long id, String name, Long comboTypeId) {
}
