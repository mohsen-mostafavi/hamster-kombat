package com.mostafavi.dto;

public record UpdateComboTypeDTO(Long id, String name) {
}
