package com.mostafavi.dto;

public record ComboDTO(Long id, String name, Long comboTypeId, String comboTypeName) {
}
