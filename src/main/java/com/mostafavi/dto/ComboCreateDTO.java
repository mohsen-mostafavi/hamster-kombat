package com.mostafavi.dto;

public record ComboCreateDTO(String name, Long comboTypeId) {
}
