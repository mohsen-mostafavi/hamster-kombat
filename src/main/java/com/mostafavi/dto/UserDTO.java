package com.mostafavi.dto;

public record UserDTO(Long id, String name) {
}
