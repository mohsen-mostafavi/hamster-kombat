package com.mostafavi.dto;

public record CreateComboLevelDTO(Long comboLevelId, Long comboId, int level, Long price, Long profit) {
}
