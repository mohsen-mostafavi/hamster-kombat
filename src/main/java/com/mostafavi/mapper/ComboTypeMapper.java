package com.mostafavi.mapper;

import com.mostafavi.dto.ComboTypeDTO;
import com.mostafavi.entity.ComboType;

public class ComboTypeMapper {

    public static ComboTypeDTO entityToDTO(ComboType comboType) {
        return new ComboTypeDTO(comboType.getId(), comboType.getName());
    }
}
