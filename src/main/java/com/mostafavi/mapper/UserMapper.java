package com.mostafavi.mapper;

import com.mostafavi.dto.UserDTO;
import com.mostafavi.entity.User;

public class UserMapper {
    public static UserDTO entityToDTO(User byName) {
        return new UserDTO(byName.getId(), byName.getName());
    }
}
