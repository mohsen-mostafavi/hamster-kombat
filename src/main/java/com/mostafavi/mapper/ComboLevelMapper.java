package com.mostafavi.mapper;

import com.mostafavi.dto.ComboLevelDTO;
import com.mostafavi.dto.CreateComboLevelDTO;
import com.mostafavi.entity.Combo;
import com.mostafavi.entity.ComboLevel;

public class ComboLevelMapper {
    public static ComboLevelDTO entityTODTO(ComboLevel comboLevel) {
        return new ComboLevelDTO(
                comboLevel.getId(),
                comboLevel.getLevel(),
                comboLevel.getPrice(),
                comboLevel.getProfit(),
                comboLevel.getCombo().getId(),
                comboLevel.getCombo().getName()
        );
    }

    public static ComboLevel dtoToEntity(CreateComboLevelDTO createComboLevelDTO, Combo combo) {
        return ComboLevel.builder()
                .combo(combo)
                .level(createComboLevelDTO.level())
                .price(createComboLevelDTO.price())
                .profit(createComboLevelDTO.profit())
                .build();
    }
}
