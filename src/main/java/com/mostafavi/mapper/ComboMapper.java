package com.mostafavi.mapper;

import com.mostafavi.dto.ComboDTO;
import com.mostafavi.entity.Combo;

public class ComboMapper {
    public static ComboDTO entityTODTO(Combo combo) {
        return new ComboDTO(combo.getId(),combo.getName(),combo.getComboType().getId(),combo.getComboType().getName());
    }
}
