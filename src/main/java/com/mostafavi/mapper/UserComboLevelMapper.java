package com.mostafavi.mapper;

import com.mostafavi.dto.UserComboLevelDTO;
import com.mostafavi.entity.UserComboLevel;

public class UserComboLevelMapper {
    public static UserComboLevelDTO entityTODTO(UserComboLevel userComboLevel) {
        return new UserComboLevelDTO(userComboLevel.getId(), userComboLevel.getUser().getId(), userComboLevel.getUser().getName(),
                userComboLevel.getComboLevel().getCombo().getId(),
                userComboLevel.getComboLevel().getCombo().getName(),
                userComboLevel.getComboLevel().getCombo().getComboType().getName(),
                userComboLevel.getComboLevel().getLevel(),
                ((double) userComboLevel.getComboLevel().getProfit() / userComboLevel.getComboLevel().getPrice()),
                userComboLevel.getComboLevel().getPrice(),
                userComboLevel.getComboLevel().getProfit());
    }
}
