package com.mostafavi.dao;

import com.mostafavi.entity.ComboLevel;
import com.mostafavi.utils.QueryUtil;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class ComboLevelDAO implements PanacheRepository<ComboLevel> {

    public List<ComboLevel> findByComboIdAndLevel(Long comboId, Integer level) {
        Map<String, Object> filter = filter(comboId, level);
        if (filter.isEmpty()) {
            return findAll().list();
        } else {
            String query = QueryUtil.createQuery(queryParam(comboId, level));
            return find(query, filter).list();
        }
    }

    private Map<String, Object> queryParam(Long comboId, Integer level) {
        Map<String, Object> params = new HashMap<>();
        if (comboId != null)
            params.put("combo.id", "comboId");
        if (level != null)
            params.put("level", "level");
        return params;
    }

    private Map<String, Object> filter(Long comboId, Integer level) {
        Map<String, Object> params = new HashMap<>();
        if (comboId != null)
            params.put("comboId", comboId);
        if (level != null)
            params.put("level", level);
        return params;
    }
}
