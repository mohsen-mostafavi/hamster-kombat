package com.mostafavi.dao;

import com.mostafavi.entity.Combo;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class ComboDAO implements PanacheRepository<Combo> {
    public List<Combo> findByType(Long typeId) {
        return find("comboType.id", typeId).list();
    }
}
