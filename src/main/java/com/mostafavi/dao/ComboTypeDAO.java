package com.mostafavi.dao;

import com.mostafavi.entity.ComboType;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ComboTypeDAO implements PanacheRepository<ComboType> {
}
