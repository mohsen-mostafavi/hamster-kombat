package com.mostafavi.dao;

import com.mostafavi.entity.UserComboLevel;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class UserComboLevelDAO implements PanacheRepository<UserComboLevel> {
    // todo: select max level
    public List<UserComboLevel> findByUserId(Long userId) {
        return find("#findByUserId",userId).list();
    }
}
